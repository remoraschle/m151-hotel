package rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class HotelClient {
	private HotelInterface inter = null;
	
	public HotelClient() {
		try {
			inter = (HotelInterface) Naming.lookup("rmi://127.0.0.1:1099/HotelServer");
			System.out.println(inter.getPersons().get(1).getGivenname());
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}
	
	public HotelInterface getHotelInterface() {
		return inter;
	}
}
