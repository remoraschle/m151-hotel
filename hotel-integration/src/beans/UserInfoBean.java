package beans;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import rmi.HotelClient;
import rmi.HotelInterface;
import models.Booking;
import models.Person;

@ManagedBean
@ViewScoped
public class UserInfoBean {
	private HotelInterface inter = null;
	private Person person = null;
	private ArrayList<Booking> booking = null;
	
	public UserInfoBean() {
		//Das händling falls keine Person mit dieser ID existiert ist nicht abgefangen.
		Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer id = Integer.parseInt(params.get("id"));
		
		HotelClient client = new HotelClient();
		inter = client.getHotelInterface();
		try {
			person = inter.getPerson(id);
			booking = inter.getBookingsForPerson(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public ArrayList<Booking> getBooking() {
		return booking;
	}

	public void setBooking(ArrayList<Booking> booking) {
		this.booking = booking;
	}
}
