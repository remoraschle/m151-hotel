package database;

import java.util.ArrayList;

import models.Booking;

public class HotelDBManagerTest {
	public HotelDBManagerTest() {
//		ArrayList<Person> persons = HotelDBManager.getHotelDBManager().getPersons("siegfried laud");
//		for(Person person : persons) {
//			System.out.println(person.getSurname() + ", " + person.getGivenname());
//		}
		
		ArrayList<Booking> bookings = HotelDBManager.getHotelDBManager().getBookingsForPerson(7);
		for(Booking booking : bookings) {
			System.out.println(booking.getPositions().get(0).getPrice());
		}
	}
	
	public static void main(String[] args) {
		new HotelDBManagerTest();
	}
}
