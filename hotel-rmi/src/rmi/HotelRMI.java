package rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;

import models.Booking;
import models.Person;
import models.Position;
import database.HotelDBManager;

public class HotelRMI extends UnicastRemoteObject implements HotelInterface {
	private static final long serialVersionUID = 1L;
	private HotelDBManager manger = null;
	
	public HotelRMI() throws RemoteException {
		try {
			manger = HotelDBManager.getHotelDBManager();
			
			LocateRegistry.createRegistry(1099);
		} catch (RemoteException e1) {
			System.out.println("Registry already started");
		}
		
		try {
			Naming.rebind("rmi://127.0.0.1/HotelServer", this);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public ArrayList<Person> getPersons() {
		return manger.getPersons();
	}

	@Override
	public ArrayList<Person> getPersons(String name) {
		return manger.getPersons(name);
	}

	@Override
	public ArrayList<Booking> getBookingsForPerson(int id) {
		return manger.getBookingsForPerson(id);
	}

	@Override
	public ArrayList<Position> getPositionsForServiceType(int serviceTypeId,
			Date from, Date to) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Person getPerson(int id) throws RemoteException {
		return manger.getPerson(id);
	}
}
