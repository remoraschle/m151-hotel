package rmi;

import java.rmi.RemoteException;

public class HotelRMIServer {

	public static void main(String[] args) {
		try {
			new HotelRMI();
			System.out.println("Service started sucessfull!");
		} catch (RemoteException e) {
			System.out.println("Failed to start service!");
			e.printStackTrace();
		}
		
	}

}
